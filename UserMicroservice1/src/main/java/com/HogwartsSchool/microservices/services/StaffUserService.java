package com.HogwartsSchool.microservices.services;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import com.HogwartsSchool.microservices.model.PatchAddressAndContactUpdate;
import com.HogwartsSchool.microservices.model.Staff;
import com.HogwartsSchool.microservices.model.StudentProspects;




public interface StaffUserService {

	// Staff Methods
		public ResponseEntity<List< Staff >> getAllStaff ();
		public ResponseEntity< Staff > insertStaff (Staff staff);
		public ResponseEntity< Staff > findStaffById(int id);
		public ResponseEntity< Staff > fullUpdateStaffById (Staff staff,int id);
		public ResponseEntity<Staff> partialUpdateStaffById( PatchAddressAndContactUpdate staffDetail, @PathVariable int id );
	    public ResponseEntity <?> deleteStaffById( int id);
		public ResponseEntity< StudentProspects> insertProspectiveStudent (StudentProspects studentProspect);
		public ResponseEntity<List<StudentProspects>> filterStudentProspects(boolean status) ;
		
}
