package com.HogwartsSchool.microservices.services;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.HogwartsSchool.microservices.exceptions.StudentNotFoundException;
import com.HogwartsSchool.microservices.model.PatchAddressAndContactUpdate;
import com.HogwartsSchool.microservices.model.Student;
import com.HogwartsSchool.microservices.repositories.StudentRepository;

@Service
public class StudentUserServiceImpl implements StudentUserService {

	@Autowired
	StudentRepository studentrepository;

	@Override
	public ResponseEntity<List<Student>> getAllStudents() {
		return ResponseEntity.ok(studentrepository.findAll()) ;
	}

	@Override
	public ResponseEntity<Student> insertStudent(Student student) {
		studentrepository.save(student);
		URI location =ServletUriComponentsBuilder
		.fromCurrentRequest()
		.path("/{id}")
		.buildAndExpand(student.getStudentId()).toUri();
		
		return ResponseEntity.created(location).body(student);
		
		
	}

	@Override
	public ResponseEntity<Student> findStudentById(int id) {
		Student student = studentrepository.findById(id)
				.orElseThrow(()-> new StudentNotFoundException(id));	
		return ResponseEntity.ok(student);
	}

	@Override
	public ResponseEntity<Student> fullUpdateStudentById(Student student, int id) {
		
		Student studentdetail = studentrepository.findById(id).orElseThrow(()-> new StudentNotFoundException (id));
		studentdetail.setStudentId(student.getStudentId());
		studentdetail.setFirstName(student.getFirstName());
		studentdetail.setLastName(student.getLastName());
		studentdetail.setDateOfBirth(student.getDateOfBirth());
		studentdetail.setGender(student.getGender());
		studentdetail.setFatherName(student.getFatherName());
        studentdetail.setAddress(student.getAddress());
        studentdetail.setContactNumber(student.getContactNumber());

        Student updatedcourse=studentrepository.save(studentdetail);
		return ResponseEntity.ok(updatedcourse);
	}

	@Override
	public ResponseEntity<Student> partialUpdateStudentById( PatchAddressAndContactUpdate studentDetail, @PathVariable int id ){
		
	    Student student = studentrepository.findById(id).orElseThrow(()-> new StudentNotFoundException(id));
	    student.setAddress(studentDetail.getAddress());
	    student.setContactNumber(studentDetail.getContactNumber());
	    
	    Student updatedcourse=studentrepository.save(student);
		return ResponseEntity.ok(updatedcourse);
	    }
	

	@Override
	public ResponseEntity <Student> deleteStudentById(@PathVariable int id) {
		Student student =studentrepository.findById(id).orElseThrow(()-> new StudentNotFoundException(id));
		studentrepository.delete(student);
		//return ResponseEntity.ok().body("successfully deleted student with id" + id + " from database");
	return ResponseEntity.ok(student);
	}
}