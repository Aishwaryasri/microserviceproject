package com.HogwartsSchool.microservices.services;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import com.HogwartsSchool.microservices.model.PatchAddressAndContactUpdate;
import com.HogwartsSchool.microservices.model.Student;




public interface StudentUserService {

	// Student Methods
	public  ResponseEntity<List<Student >> getAllStudents();
	public  ResponseEntity< Student > insertStudent(Student student);
	public  ResponseEntity< Student > findStudentById(int id);
	public  ResponseEntity< Student > fullUpdateStudentById (Student student ,int id);
	public  ResponseEntity<Student> partialUpdateStudentById( PatchAddressAndContactUpdate studentDetail, @PathVariable int id );
	public ResponseEntity <Student> deleteStudentById(int id);
	
	

}
