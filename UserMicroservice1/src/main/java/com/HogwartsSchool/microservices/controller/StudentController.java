package com.HogwartsSchool.microservices.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.HogwartsSchool.microservices.exceptions.StudentNotFoundException;
import com.HogwartsSchool.microservices.model.PatchAddressAndContactUpdate;
import com.HogwartsSchool.microservices.model.Student;
import com.HogwartsSchool.microservices.services.StudentUserService;

@RestController
public class StudentController {

	@Autowired 
	private StudentUserService studentuserservice;
	

// Get All Student
	    @GetMapping(value="/user-api/students/all-students")
	    public ResponseEntity<List<Student>> getAllStudents(){
	    	return studentuserservice.getAllStudents();
	    }

// Create a new Student
	    @PostMapping(value="/user-api/students/insert-student")
	    public ResponseEntity<Student > insertStudent(@RequestBody Student student){
	    	return studentuserservice.insertStudent(student);
	    }
// Get a Single student detail
	    @GetMapping(value="/user-api/students/find-student-by-id/{id}")
	    public ResponseEntity<Student > findStudentById(@PathVariable int id) throws StudentNotFoundException{
	    	return studentuserservice.findStudentById(id);
	    }
// full update student record
	    @PutMapping("/user-api/students/full-update-student-record-by-id/{id}")
	    public ResponseEntity<Student > fullUpdateStudentById(@RequestBody Student student, @PathVariable int id) throws StudentNotFoundException{
	    	return studentuserservice.fullUpdateStudentById(student,id);             
	    }
	    

//partial update student record
	  @PatchMapping(value = "/user-api/students/partial-update-student-record-by-id/{id}")
		public ResponseEntity<Student> partialUpdateStudentById( @RequestBody PatchAddressAndContactUpdate studentDetail, @PathVariable int id ){
	    	   return studentuserservice.partialUpdateStudentById(studentDetail,id);   
	       }

	    
// Delete a student detail
	    @DeleteMapping(value="/user-api/students/delete-student-by-id/{id}")
	    public ResponseEntity <Student> deleteStudentById(@PathVariable int id) throws StudentNotFoundException{
	    	return studentuserservice.deleteStudentById(id);
	    }


	  
	    
		   
	    
}
