package com.HogwartsSchool.microservices.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.HogwartsSchool.microservices.model.PatchAddressAndContactUpdate;
import com.HogwartsSchool.microservices.model.Staff;
import com.HogwartsSchool.microservices.model.StudentProspects;
import com.HogwartsSchool.microservices.services.StaffUserService;

@RestController
public class StaffController {

@Autowired StaffUserService staffuserservice;
	
	
	// Create a new Staff
    @PostMapping(value="/user-api/staff/insert-staff")
    public ResponseEntity<Staff> insertStaff(@RequestBody Staff staff) {
    	return staffuserservice.insertStaff(staff);
    }
//Get a Single Note
    @GetMapping(value="/user-api/staff/find-staff-by-id/{id}")
    public ResponseEntity<Staff> findStaffById(@PathVariable int id){	    	
    return staffuserservice.findStaffById(id);
    }
//full update staff record
    @PutMapping("/user-api/staff/full-update-staff-record-by-id/{id}")
    public ResponseEntity<Staff> fullUpdateStaffById(@RequestBody Staff staff, @PathVariable int id) {
          return staffuserservice.fullUpdateStaffById(staff,id);             
    }
    
//partial update staff record

 
   @PatchMapping(value = "/user-api/staff/partial-update-staff-record-by-id/{id}")
	public ResponseEntity<Staff> partialUpdateStaffById( @RequestBody PatchAddressAndContactUpdate staffDetail, @PathVariable int id ){
    	   return staffuserservice.partialUpdateStaffById(staffDetail,id);  
   }
  
    
//Delete a Note
    @DeleteMapping(value="/user-api/staff/delete-staff-by-id/{id}")
    public ResponseEntity <?> deleteStaffById(@PathVariable int id){
    	return staffuserservice.deleteStaffById(id);
    }

//Adding a new student by staff 
    @PostMapping(value="/user-api/staff/insert-prospective-student")
    
    public ResponseEntity<StudentProspects>insertProspectiveStudent(@Valid @RequestBody StudentProspects studentProspects){
    	return staffuserservice.insertProspectiveStudent(studentProspects);
    	
    }
    
//Filtering student prospects
    @GetMapping(value="/user-api/staff/filter-student-prospects/{status}")
	public ResponseEntity<List<StudentProspects>> filterStudentProspects(boolean status) {
    return staffuserservice.filterStudentProspects(status);
    }
  
    
}
