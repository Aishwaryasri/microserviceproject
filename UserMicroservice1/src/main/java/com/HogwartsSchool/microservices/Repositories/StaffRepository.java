package com.HogwartsSchool.microservices.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.HogwartsSchool.microservices.model.Staff;

@Repository
public interface StaffRepository extends JpaRepository<Staff, Integer>{

}
	

