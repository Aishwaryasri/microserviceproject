package com.HogwartsSchool.microservices.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.HogwartsSchool.microservices.model.Assessments;

@Repository

	public interface AssessmentRepository extends JpaRepository<Assessments, Integer>{

}