package com.HogwartsSchool.microservices.model;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class PatchAddressAndContactUpdate {

@Size(min = 3, max = 500, message = "Address name should bee atleast 3 characters and maximum of 500 characters ")
@Column(name = "address")
private String address;


@NotNull(message= "phoneNo cannot be empty")
@Pattern(regexp="(^$|[0-9] {10})", message = "please enter only digits")
@Size(min = 10, max = 10, message = "please enter 10-digit number")
@Column(name= "contactnumber")
private String contactNumber;

public PatchAddressAndContactUpdate() {
	 
}

public PatchAddressAndContactUpdate(String address,String contactNumber) {
	super();
	this.address = address;
	this.contactNumber = contactNumber;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getContactNumber() {
	return contactNumber;
}

public void setContactnumber(String contactNumber) {
	this.contactNumber = contactNumber;
}







}




