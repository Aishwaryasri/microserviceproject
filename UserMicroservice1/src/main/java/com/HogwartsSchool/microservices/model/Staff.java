package com.HogwartsSchool.microservices.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="Staff")
public class Staff {

	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name =	"staffid")
	private int staffId;

	@Column(name =	"firstname", nullable = false)
	private String firstName;

	@Column(name =	"lastname", nullable = false)
	private String lastName;

	@Column(name =	"dateofbirth", nullable = false)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateOfBirth;
		
	@Column(name =	"gender", nullable = false)
	private String gender;

	@Column(name =	"address", nullable = false)
	private String address;

	@Column(name =	"contactnumber", nullable = false)
	private String contactNumber;

	@Column(name =	"role", nullable = false)
	private String role;

	public Staff() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Staff(int staffId, String firstName, String lastName, Date dateOfBirth, String gender, String address,
			String contactNumber, String role) {
		super();
		this.staffId = staffId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.address = address;
		this.contactNumber = contactNumber;
		this.role = role;
	}

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
		
}
