package com.HogwartsSchool.microservices.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="studentresults")
public class StudentResults {

	  
	   @Id
	   @GeneratedValue(strategy=GenerationType.IDENTITY)   
	   @Column(name="studentresultid")
	   private int studentResultId;
	   
	   @Column(name="studentid")
	   private int StudentId;
	   
	  
	   @Column(name="assessmentid")
	   private int AssessmentId;
	  
	   @Column(name="remarks",nullable=false)
	   private String remarks;

	public StudentResults() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudentResults(int studentResultId, int studentId, int assessmentId, String remarks) {
		super();
		this.studentResultId = studentResultId;
		StudentId = studentId;
		AssessmentId = assessmentId;
		this.remarks = remarks;
	}

	public int getStudentResultId() {
		return studentResultId;
	}

	public void setStudentResultId(int studentResultId) {
		this.studentResultId = studentResultId;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public int getAssessmentId() {
		return AssessmentId;
	}

	public void setAssessmentId(int assessmentId) {
		AssessmentId = assessmentId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	   
	   
}
