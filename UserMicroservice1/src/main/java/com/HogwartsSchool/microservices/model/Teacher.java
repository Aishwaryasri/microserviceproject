package com.HogwartsSchool.microservices.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "teacher")
public class Teacher {

	 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "teacherid")
    private int teacherId;
    
    @Column(name = "firstname" , nullable = false)
    private String firstName;
    
    @Column(name = "lastname" , nullable = false)
    private String lastName;
    
    @Column(name = "dateofbirth" , nullable = false)
    Date dateOfBirth;
    
    @Column(name = "gender" , nullable = false)
    private String gender;
    
    @Column(name = "contactnumber" , nullable = false)
    private String contactNumber;
    
    @Column(name = "qualification" , nullable = false)
    private String qualification;
    
    @Column(name = "experienceinyears",nullable = false)
    private int ExperienceInYears;
    
    @Column(name = "address" , nullable = false)
    private String address;

	public Teacher() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Teacher(int teacherId, String firstName, String lastName, Date dateOfBirth, String gender,
			String contactNumber, String qualification, int experienceInYears, String address) {
		super();
		this.teacherId = teacherId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.contactNumber = contactNumber;
		this.qualification = qualification;
		ExperienceInYears = experienceInYears;
		this.address = address;
	}

	public int getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public int getExperienceInYears() {
		return ExperienceInYears;
	}

	public void setExperienceInYears(int experienceInYears) {
		ExperienceInYears = experienceInYears;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
    
    
}
