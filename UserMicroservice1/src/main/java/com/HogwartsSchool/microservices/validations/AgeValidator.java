

package com.HogwartsSchool.microservices.validations;


import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class AgeValidator implements ConstraintValidator<MinimumAge, Date> {


    @Override
    public void initialize(MinimumAge constraintAnnotation) {


    }


    @Override
    public boolean isValid(Date value, ConstraintValidatorContext context) {
        
        // to get current date
        LocalDate currentDate = LocalDate.now();
        int currentYear = currentDate.getYear();
        
        // to get year of birth
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(value);
        int yearOfBirth = calendar.get(Calendar.YEAR);
        
        int age = currentYear - yearOfBirth;
        if (age >= 16) {
            return true;
        } else {
            return false;
        }


    }


}









