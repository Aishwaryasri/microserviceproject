package com.HogwartsSchool.microservices.exceptions;

public class StudentResultsNotFoundException extends RuntimeException {

	private int id;
	public StudentResultsNotFoundException(int id) {
		  super(String.format("employee is not found with id : '%s'",id));
		        }
}
