package com.HogwartsSchool.microservices.student.model;

import java.util.Date;

public class Student {
	    
	    private int studentId;
	    private String firstName;
	    private String lastName;
	    Date dateOfBirth;
	    private String gender;
	    private String fatherName;
	    private String address;
	    private String contactNumber;
		public Student() {
			super();
			// TODO Auto-generated constructor stub
		}
		public Student(int studentId, String firstName, String lastName, Date dateOfBirth, String gender,
				String fatherName, String address, String contactNumber) {
			super();
			this.studentId = studentId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.dateOfBirth = dateOfBirth;
			this.gender = gender;
			this.fatherName = fatherName;
			this.address = address;
			this.contactNumber = contactNumber;
		}
		public int getStudentId() {
			return studentId;
		}
		public void setStudentId(int studentId) {
			this.studentId = studentId;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public Date getDateOfBirth() {
			return dateOfBirth;
		}
		public void setDateOfBirth(Date dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getFatherName() {
			return fatherName;
		}
		public void setFatherName(String fatherName) {
			this.fatherName = fatherName;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getContactNumber() {
			return contactNumber;
		}
		public void setContactNumber(String contactNumber) {
			this.contactNumber = contactNumber;
		}
	    
	    
}
