package com.HogwartsSchool.microservices.student;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.HogwartsSchool.microservices.student.model.Student;

@FeignClient(name = "UserMicroservice" , url = "localhost:8500")

public interface StudentMicroserviceProxy {

	@PutMapping("/user-api/students/full-update-student-record-by-id/{id}")
    public abstract ResponseEntity<Student > fullUpdateStudentById(@RequestBody Student student, @PathVariable int id);



 
}
 

