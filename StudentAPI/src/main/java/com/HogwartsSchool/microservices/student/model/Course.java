package com.HogwartsSchool.microservices.student.model;

public class Course {

	
	 private int courseId;
	 private String courseName;

	 private String coursePreRequisite;
	 private String courseDescription;
	 private int credits;
	public Course() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Course(int courseId, String courseName, String coursePreRequisite, String courseDescription, int credits) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.coursePreRequisite = coursePreRequisite;
		this.courseDescription = courseDescription;
		this.credits = credits;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCoursePreRequisite() {
		return coursePreRequisite;
	}
	public void setCoursePreRequisite(String coursePreRequisite) {
		this.coursePreRequisite = coursePreRequisite;
	}
	public String getCourseDescription() {
		return courseDescription;
	}
	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}
	public int getCredits() {
		return credits;
	}
	public void setCredits(int credits) {
		this.credits = credits;
	}

}
