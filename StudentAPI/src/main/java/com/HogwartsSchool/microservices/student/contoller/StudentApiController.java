package com.HogwartsSchool.microservices.student.contoller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.HogwartsSchool.microservices.student.CourseMicroserviceProxy;
import com.HogwartsSchool.microservices.student.StudentMicroserviceProxy;
import com.HogwartsSchool.microservices.student.model.Course;
import com.HogwartsSchool.microservices.student.model.Student;

@RestController
public class StudentApiController {
    
    @Autowired
    CourseMicroserviceProxy courseMicroserviceProxy;
    @Autowired
    StudentMicroserviceProxy studentMicroserviceProxy;
    
    @GetMapping("/student-service/courses")
    public ResponseEntity<List<Course>>getAllCourses(){
        return courseMicroserviceProxy.getAllCourses();
    }

 

    @PutMapping("/student-service/{id}")
    public ResponseEntity<Student> updateStudentDetails (@RequestBody Student student,@PathVariable int id ){
        return studentMicroserviceProxy.fullUpdateStudentById(student, id);
    }
}