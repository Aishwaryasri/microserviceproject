package com.HogwartsSchool.microservices.student;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import com.HogwartsSchool.microservices.student.model.Course;

@FeignClient(name = "CourseMicroservice" , url = "localhost:8090")

public interface CourseMicroserviceProxy {
	@GetMapping(value="/user-api/staff/all-staff")
    public ResponseEntity<List<Course>>getAllCourses();
    
}

