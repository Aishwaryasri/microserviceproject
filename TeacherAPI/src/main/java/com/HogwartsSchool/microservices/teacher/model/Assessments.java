
package com.HogwartsSchool.microservices.teacher.model;


import java.sql.Date;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonFormat;



@Entity
@Table(name = "assessments")
public class Assessments {
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "assessmentid")
    private int assessmentId;
    
    @Column(name= "teacherid" )
    private int teacherId;
    
    @Column(name = "assessmentname", nullable = false)
    private String assessmentName;
    
    @Column(name = "assessmentlink", nullable = false)
    private String assessmentLink;
    
    @Column(name = "deadline", nullable = false)
    Date deadline;


    
    public Assessments() {
    }
    
    
    
    public Assessments(int assessmentId, int teacherId, String assessmentName, String assessmentLink,
            Date deadline) {
        super();
        this.assessmentId = assessmentId;
        this.teacherId = teacherId;
        this.assessmentName = assessmentName;
        this.assessmentLink = assessmentLink;
        this.deadline = deadline;
    }


    public int getAssessmentId() {
        return assessmentId;
    }


    public void setAssessmentId(int assessmentId) {
        this.assessmentId = assessmentId;
    }


    public int getTeacherId() {
        return teacherId;
    }


    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }


    public String getAssessmentName() {
        return assessmentName;
    }


    public void setAssessmentName(String assessmentName) {
        this.assessmentName = assessmentName;
    }


    public String getAssessmentLink() {
        return assessmentLink;
    }


    public void setAssessmentLink(String assessmentLink) {
        this.assessmentLink = assessmentLink;
    }


    public Date getDeadline() {
        return deadline;
    }


    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }
    
    
}
















