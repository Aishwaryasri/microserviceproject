package com.HogwartsSchool.microservices.teacher.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.HogwartsSchool.microservices.teacher.TeacherMicroserviceProxy;
import com.HogwartsSchool.microservices.teacher.model.Assessments;
import com.HogwartsSchool.microservices.teacher.model.StudentResults;
import com.HogwartsSchool.microservices.teacher.model.Teacher;


@RestController
public class TeacherServiceController {
	
	@Autowired 
	private TeacherMicroserviceProxy teachermicroserviceproxy;

	
	// Get All Notes
    @GetMapping(value="/teacher-service/{id}")
    public ResponseEntity< Teacher> getTeacherByID(@PathVariable int id) {
    	return teachermicroserviceproxy.findTeacherById(id);
    }
    
    @PutMapping(value="/teacher-service/{id}") 
    public ResponseEntity< Teacher > updateTeacherDetails (@RequestBody Teacher teacher, @PathVariable int id) {
    	return teachermicroserviceproxy.fullUpdateTeacherById(teacher,id);
    }
    
    
 
    @PostMapping(value="/teacher-service/add-assessment")
    public ResponseEntity< Assessments > addAssessment(@RequestBody Assessments Assessment) {
    	return teachermicroserviceproxy.insertAssessment(Assessment);
    }
    
    @PostMapping(value="/teacher-service/add-assessment-results")
   public ResponseEntity<StudentResults > addAssessmentResults (@RequestBody StudentResults studentResults) {
    	return teachermicroserviceproxy.insertAssessmentResults(studentResults);
    }
    
}
