package com.HogwartsSchool.microservices.teacher;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.HogwartsSchool.microservices.teacher.model.Assessments;
import com.HogwartsSchool.microservices.teacher.model.StudentResults;
import com.HogwartsSchool.microservices.teacher.model.Teacher;

@FeignClient(name = "UserMicroservice2" , url = "localhost:8500")
public interface TeacherMicroserviceProxy {
	 @GetMapping(value="/user-api/teachers/find-teacher-by-id/{id}")
	    public ResponseEntity<Teacher> findTeacherById(@PathVariable int id);
	 

	 
	 @PutMapping("/user-api/teachers/full-update-teacher-record-by-id/{id}") 
	    public ResponseEntity<Teacher> fullUpdateTeacherById(@RequestBody Teacher teacher, @PathVariable int id);
	 
	 @PostMapping(value="/user-api/teachers/insert-assessment")
	    public ResponseEntity<Assessments> insertAssessment(@RequestBody Assessments Assessment);
	  	    
	    


	 
	    @PostMapping(value="/user-api/teachers/insert-assessment-results")
		public ResponseEntity<StudentResults> insertAssessmentResults(@RequestBody StudentResults studentResults);

	
	
	
	 
	 
	 
	
	}
	 
	