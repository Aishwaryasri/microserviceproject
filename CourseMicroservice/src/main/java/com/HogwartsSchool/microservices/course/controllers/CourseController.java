package com.HogwartsSchool.microservices.course.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.HogwartsSchool.microservices.course.exceptions.CourseNotFoundException;
import com.HogwartsSchool.microservices.course.model.Course;
import com.HogwartsSchool.microservices.course.service.CourseService;

@RestController
public class CourseController {

		@Autowired 
		private CourseService courseservice;
		

// Get All Staff
    @GetMapping(value="/user-api/staff/all-staff")
    public ResponseEntity<List<Course>>getAllCourses() {
        return courseservice.getAllCourses();
    }

    
// Create a new course
    @PostMapping(value="/course-api/insert-course")
    public ResponseEntity<Course >insertCourse(@RequestBody Course course){
			return courseservice.insertCourse(course);
        	
        }

// Get a course detail by id
     @GetMapping(value="/course-api /get-course-details-by-id/{id}")
     public ResponseEntity<Course > findCourseById(@PathVariable int id){
    	 return courseservice.findCourseById(id);
         }
     
     

 /*// Get specific course info by id
     @GetMapping("/course-api /get-specific-course-info/{id}/{info-attribute}")
     public ResponseEntity<Course> findSpecificCourseInfo(@PathVariable int id, String infoAttribute) {
    	return courseservice.findSpecificCourseInfo(id, infoAttribute);
     }*/
     
 //update a course by id
     @PutMapping(value="/course-api /update-course-by-id/{id}")
     public ResponseEntity<Course> updateCourse(@RequestBody Course course, @PathVariable int id) throws CourseNotFoundException {   
     return courseservice.updateCourse(course, id) ;
     }
    
 //Delete course by id 
     @DeleteMapping("/course-api /delete-course-by-id/{id}")
     public ResponseEntity <?> deleteCourse(@PathVariable int id) throws CourseNotFoundException{
        return courseservice.deleteCourse(id) ;
        }
   
}
