package com.HogwartsSchool.microservices.course.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.HogwartsSchool.microservices.course.model.Course;

public interface CourseService {
	//Course Methods
	public ResponseEntity<List<Course>>getAllCourses();
	public ResponseEntity<Course> insertCourse(Course course);
	public ResponseEntity<Course> findCourseById(int courseId);
	/*public ResponseEntity<Course> findSpecificCourseInfo (int id, String infoAttribute);*/
	public ResponseEntity<Course> updateCourse(Course course, int id);
	public ResponseEntity<?> deleteCourse(int id);
	
	}
 