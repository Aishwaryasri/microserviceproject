package com.HogwartsSchool.microservices.course.exceptions;

public class CourseNotFoundException extends RuntimeException  {
	private int id;
	public CourseNotFoundException(int id) {
		  super(String.format("employee is not found with id : '%s'",id));
		        }
	}