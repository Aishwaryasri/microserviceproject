package com.HogwartsSchool.microservices.course.service;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.HogwartsSchool.microservices.course.Repositories.CourseRepository;
import com.HogwartsSchool.microservices.course.exceptions.CourseNotFoundException;
import com.HogwartsSchool.microservices.course.model.Course;


@Service
public class CourseServiceImpl  implements CourseService{

@Autowired
CourseRepository courserepository;
@Override
public ResponseEntity<List<Course>> getAllCourses() {
	return ResponseEntity.ok(courserepository.findAll()) ;
}

@Override
public ResponseEntity<Course> insertCourse(Course course) {
	courserepository.save(course);
	URI location =ServletUriComponentsBuilder
	.fromCurrentRequest()
	.path("/{id}")
	.buildAndExpand(course.getCourseId()).toUri();
	
	return ResponseEntity.created(location).body(course);
}

@Override
public ResponseEntity<Course> findCourseById(int id) {
	Course course = courserepository.findById(id)
			.orElseThrow(()-> new CourseNotFoundException(id));	
	return ResponseEntity.ok(course);
}
/*
@Override
public ResponseEntity<Course> findSpecificCourseInfo(int id, String infoAttribute) {
	Course course = courserepository.findById(id)
			.orElseThrow(()-> new CourseNotFoundException("course not found - " + id));	
	return ResponseEntity.ok());
}
*/

@Override
public ResponseEntity<Course> updateCourse(Course course, int id){
	Course coursedetail = courserepository.findById(id).orElseThrow(()-> new CourseNotFoundException(id));
	coursedetail.setCourseId(course.getCourseId());
	coursedetail.setCourseName(course.getCourseName());
	coursedetail.setCoursePreRequisite(course.getCoursePreRequisite());
	coursedetail.setCourseDescription(course.getCourseDescription());
	coursedetail.setCredits(course.getCredits());
	Course updatedcourse=courserepository.save(coursedetail);
	return ResponseEntity.ok(updatedcourse);
	
	
}

@Override
public ResponseEntity<?> deleteCourse(@PathVariable int id) {

	Course course =courserepository.findById(id).orElseThrow(()-> new CourseNotFoundException(id));
	courserepository.delete(course);
	return ResponseEntity.ok().body("Successfully deleted player with id: "+id + " from database ");
}
}



