package com.HogwartsSchool.microservices.course.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.HogwartsSchool.microservices.course.model.Course;
@Repository
public interface CourseRepository extends JpaRepository<Course, Integer>{


}