package com.HogwartsSchool.microservices.course.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="Course")
public class Course {

 @Id
 @GeneratedValue(strategy=GenerationType.AUTO)
 @Column(name =	"courseid")
 private int courseId;
 
 @Column(name =	"coursename", nullable = false)
private String courseName;
 
 @Column(name =	"courseprerequisite", nullable = false)
private String coursePreRequisite;
 
 @Column(name =	"coursedescription", nullable = false)
private String courseDescription;
 
 @Column(name =	"credits", nullable = false)
private int credits;
	
	public Course(){
		
		super();
		
	}
	
	public Course(int courseId, String courseName, String coursePreRequisite, String courseDescription, int credits) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.coursePreRequisite = coursePreRequisite;
		this.courseDescription = courseDescription;
		this.credits = credits;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCoursePreRequisite() {
		return coursePreRequisite;
	}
	public void setCoursePreRequisite(String coursePreRequisite) {
		this.coursePreRequisite = coursePreRequisite;
	}
	public String getCourseDescription() {
		return courseDescription;
	}
	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}
	public int getCredits() {
		return credits;
	}
	public void setCredits(int credits) {
		this.credits = credits;
	}
	

}
