package com.HogwartsSchool.microservices.staff.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.HogwartsSchool.microservices.staff.StudentMicroserviceProxy;
import com.HogwartsSchool.microservices.staff.StudentProspectsMicroserviceProxy;
import com.HogwartsSchool.microservices.staff.model.Student;
import com.HogwartsSchool.microservices.staff.model.StudentProspects;


@RestController
class StaffController  {
	  
    @Autowired
    StudentProspectsMicroserviceProxy studentProspectMicroserviceProxy;
    @Autowired
    StudentMicroserviceProxy studentMicroserviceProxy;
    
    @PostMapping("/staff-service/add-prospective-students")
    public  ResponseEntity<StudentProspects>addProspectiveStudent(@RequestBody StudentProspects studentProspect){
        return studentProspectMicroserviceProxy.insertProspectiveStudent(studentProspect);
    }

 

    @PostMapping("/staff-service/add-student")
    public ResponseEntity<Student>addStudent(@RequestBody Student student){
        return studentMicroserviceProxy.insertStudent(student);
    }
    
    @DeleteMapping("/staff-service/remove-student-by-id/{id}")
    public ResponseEntity<Student>removeStudent(@PathVariable int id){
        return studentMicroserviceProxy.deleteStudentById(id);
    }
    
    
    @PutMapping("/staff-service/update-student-by-id/{id}")
    public ResponseEntity<Student>updateStudentDetails(@RequestBody Student student, @PathVariable int id){
        return studentMicroserviceProxy.fullUpdateStudentById(student,id);
    }
    
    @GetMapping("/staff-service/filter-prospectives/{status}")
    public ResponseEntity<List<StudentProspects>> filterStudentProspects(@PathVariable String status){
        return studentProspectMicroserviceProxy.filterStudentProspects(status);
    }
}
