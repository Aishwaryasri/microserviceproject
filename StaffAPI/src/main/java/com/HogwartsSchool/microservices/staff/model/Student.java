package com.HogwartsSchool.microservices.staff.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name ="Student")
public class Student {

@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name =	"studentid")
private int studentId;

@Column(name =	"firstname", nullable = false)
private String firstName;

@Column(name =	"lastname", nullable = false)
private String lastName;

@Column(name =	"dateofbirth", nullable = false)
private Date dateOfBirth;

@Column(name =	"gender", nullable = false)
private String Gender;

@Column(name =	"fathername", nullable = false)
private String fatherName;

@Column(name =	"address", nullable = false)
private String Address;

@Column(name =	"contactnumber", nullable = false)
private String contactNumber;
	
	
	public  Student()
	{
		
		super();
	}
	public Student(int studentId, String firstName, String lastName, Date dateOfBirth, String gender,
			String fatherName, String address, String contactNumber) {
		super();
		this.studentId = studentId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		Gender = gender;
		this.fatherName = fatherName;
		Address = address;
		this.contactNumber = contactNumber;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	

}
	 
