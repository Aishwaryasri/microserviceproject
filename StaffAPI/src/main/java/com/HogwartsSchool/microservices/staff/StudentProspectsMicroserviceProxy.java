package com.HogwartsSchool.microservices.staff;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.HogwartsSchool.microservices.staff.model.StudentProspects;

 @FeignClient(name = "UserMicroservice4" , url = "localhost:8500")
public interface StudentProspectsMicroserviceProxy {
	 
    @PostMapping("/user-api/staff/insert-prospective-student")
    public ResponseEntity<StudentProspects> insertProspectiveStudent(@RequestBody StudentProspects studentProspect);
    
    @GetMapping("/user-api/staff/filter-student-prospects/{status}")
    public ResponseEntity<List<StudentProspects>>filterStudentProspects(@PathVariable String status);
}