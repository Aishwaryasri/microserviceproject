package com.HogwartsSchool.microservices.staff;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.HogwartsSchool.microservices.staff.model.Student;

 


@FeignClient(name = "UserMicroservice3" , url = "localhost:8500")
public interface StudentMicroserviceProxy {
    
    @PostMapping("/user-api/students/insert-student")
    public ResponseEntity<Student > insertStudent(@RequestBody Student student);
    
    @DeleteMapping("/user-api/students/delete-student-by-id/{id}")
    public ResponseEntity<Student> deleteStudentById(@PathVariable int id);
    
    @PutMapping("/user-api/students/full-update-student-record-by-id/{id}")
    public ResponseEntity<Student>fullUpdateStudentById(@RequestBody Student student, @PathVariable int id);

}