package com.HogwartsSchool.microservices.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.HogwartsSchool.microservices.model.StudentResults;

@Repository
	
	public interface StudentResultsRepository extends JpaRepository<StudentResults, Integer>{
}
