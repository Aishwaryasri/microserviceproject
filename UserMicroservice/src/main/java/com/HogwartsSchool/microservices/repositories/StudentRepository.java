package com.HogwartsSchool.microservices.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.HogwartsSchool.microservices.model.Student;
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>{

}
	