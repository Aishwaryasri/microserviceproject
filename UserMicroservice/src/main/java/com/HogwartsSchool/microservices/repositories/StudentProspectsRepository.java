package com.HogwartsSchool.microservices.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.HogwartsSchool.microservices.model.StudentProspects;


@Repository
public interface StudentProspectsRepository extends JpaRepository<StudentProspects, Integer> 
    {

 

    @Query(value ="SELECT * FROM studentprospects s WHERE s.status = true ",nativeQuery = true)
    public List<StudentProspects> findByActive(Boolean active);
    
    
    @Query(value ="SELECT * FROM studentprospects s WHERE s.status = false ",nativeQuery = true)
    public List<StudentProspects> findByPassive(Boolean passive);
    
    
    }
