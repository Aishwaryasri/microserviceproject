package com.HogwartsSchool.microservices.exceptions;

public class StaffNotFoundException extends RuntimeException  {

		private int id;
		public StaffNotFoundException(int id) {
			  super(String.format("employee is not found with id : '%s'",id));
			        }
		}

