package com.HogwartsSchool.microservices.exceptions;

public class TeacherNotFoundException extends RuntimeException {

	private int id;
	public TeacherNotFoundException(int id) {
		  super(String.format("employee is not found with id : '%s'",id));
		        }
}
