package com.HogwartsSchool.microservices.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.HogwartsSchool.microservices.exceptions.TeacherNotFoundException;
import com.HogwartsSchool.microservices.model.Assessments;
import com.HogwartsSchool.microservices.model.PatchAddressAndContactUpdate;
import com.HogwartsSchool.microservices.model.StudentResults;
import com.HogwartsSchool.microservices.model.Teacher;
import com.HogwartsSchool.microservices.services.TeacherUserService;


@RestController
public class TeacherController {

	

@Autowired 

private TeacherUserService teacheruserservice;
			

// Get All Teacher
			    @GetMapping(value="/user-api/teachers/all-teachers")
			    public ResponseEntity< List<Teacher>> getAllTeachers(){
			    	return teacheruserservice.getAllTeachers();
			    }
// Get All Assessment
			    @GetMapping(value="/user-api/teachers/all-assessment")
			    public ResponseEntity< List<Assessments>> getAllAssessment(){
			    	return teacheruserservice.getAllAssessment();
			    }
// Create a new Teacher
			    @PostMapping(value="/user-api/teachers/insert-teacher")
			    public ResponseEntity<Teacher> insertTeacher(@RequestBody Teacher teacher){
			    	return teacheruserservice.insertTeacher(teacher);
			    }
// Get a Single Teacher Detail
			    @GetMapping(value="/user-api/teachers/find-teacher-by-id/{id}")
			    public ResponseEntity<Teacher> findTeacherById(@PathVariable int id) {
			    	return teacheruserservice.findTeacherById(id);
			    }
// full update teacher record
			    @PutMapping("/user-api/teachers/full-update-teacher-record-by-id/{id}")
			    public ResponseEntity<Teacher> fullUpdateTeacherById(@RequestBody Teacher teacher, @PathVariable int id)throws TeacherNotFoundException{
			    	return teacheruserservice.fullUpdateTeacherById(teacher,id);             
			    }
			    
//partial update teacher record
			    @PatchMapping("/user-api/teachers/partial-update-teacher-record-by-id/{id}")
			    public ResponseEntity<Teacher> partialUpdateTeacherById(@RequestBody PatchAddressAndContactUpdate teacherDetails, @PathVariable int id )
			    {
			        return teacheruserservice.partialUpdateTeacherById(teacherDetails,id);
			    }

			    
// Delete a teacher detail
			    @DeleteMapping(value="/user-api/teachers/delete-teacher-by-id/{id}")
			    public ResponseEntity <?> deleteTeacherById(@PathVariable int id) throws TeacherNotFoundException{
			    	return teacheruserservice.deleteTeacherById(id);
			    }
			    
//Create assessment
			    @PostMapping(value="/user-api/teachers/insert-assessment")
			    public ResponseEntity<Assessments> insertAssessment(@RequestBody Assessments Assessment){
			    	return teacheruserservice.insertAssessment(Assessment);
			    }		    
			    
			    

//Display Results
			    @PostMapping(value="/user-api/teachers/insert-assessment-results")
				public ResponseEntity<StudentResults> insertAssessmentResults(@RequestBody StudentResults studentResults){
			    	return teacheruserservice.insertAssessmentResults(studentResults);
			    }			
			    
			    

			  
		
}
