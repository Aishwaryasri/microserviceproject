package com.HogwartsSchool.microservices.services;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.HogwartsSchool.microservices.exceptions.StaffNotFoundException;
import com.HogwartsSchool.microservices.exceptions.StudentNotFoundException;
import com.HogwartsSchool.microservices.model.PatchAddressAndContactUpdate;
import com.HogwartsSchool.microservices.model.Staff;
import com.HogwartsSchool.microservices.model.StudentProspects;
import com.HogwartsSchool.microservices.repositories.StaffRepository;
import com.HogwartsSchool.microservices.repositories.StudentProspectsRepository;

@Service
public class StaffUserServiceImpl implements StaffUserService{
	
@Autowired
	StaffRepository staffrepository;
@Autowired
    StudentProspectsRepository studentprospectsrepository;

@Override
public ResponseEntity<List<Staff>> getAllStaff() {
	// TODO Auto-generated method stub
	return ResponseEntity.ok(staffrepository.findAll());
}

@Override
public ResponseEntity<Staff> insertStaff(Staff staff) {
	staffrepository.save(staff);
	URI location =ServletUriComponentsBuilder
	.fromCurrentRequest()
	.path("/{id}")
	.buildAndExpand(staff.getStaffId()).toUri();
	
	return ResponseEntity.created(location).body(staff);
}

@Override
public ResponseEntity<Staff> findStaffById(int id) {
	Staff staff = staffrepository.findById(id)
			.orElseThrow(()-> new StaffNotFoundException(id));	
	return ResponseEntity.ok(staff);
}

@Override
public ResponseEntity<Staff> fullUpdateStaffById(Staff staff, int id) {

	Staff staffdetail = staffrepository.findById(id).orElseThrow(()-> new StaffNotFoundException (id));
	staffdetail.setStaffId(staff.getStaffId());
	staffdetail.setFirstName(staff.getFirstName());
	staffdetail.setLastName(staff.getLastName());
	staffdetail.setDateOfBirth(staff.getDateOfBirth());
	staffdetail.setGender(staff.getGender());
	staffdetail.setAddress(staff.getAddress());
	staffdetail.setContactNumber(staff.getContactNumber());
	staffdetail.setRole(staff.getRole());


	Staff updatedcourse=staffrepository.save(staffdetail);
	return ResponseEntity.ok(updatedcourse);
}

@Override
public ResponseEntity<Staff> partialUpdateStaffById( PatchAddressAndContactUpdate staffDetail, @PathVariable int id ){
	
    Staff staff = staffrepository.findById(id).orElseThrow(()-> new StudentNotFoundException(id));
    staff.setAddress(staffDetail.getAddress());
    staff.setContactNumber(staffDetail.getContactNumber());
    
    Staff updatedcourse=staffrepository.save(staff);
	return ResponseEntity.ok(updatedcourse);
    
}
@Override
public ResponseEntity<StudentProspects> insertProspectiveStudent(@RequestBody StudentProspects studentProspects) {
	studentprospectsrepository.save(studentProspects);
	URI location =ServletUriComponentsBuilder
	.fromCurrentRequest()
	.path("/{id}")
	.buildAndExpand(studentProspects.getProspectId()).toUri();
	
	return ResponseEntity.created(location).body(studentProspects);
}

@Override
public ResponseEntity <?> deleteStaffById(@PathVariable int id){
	Staff staff =staffrepository.findById(id).orElseThrow(()-> new StudentNotFoundException(id));
	staffrepository.delete(staff);
	return ResponseEntity.ok().body("successfully deleted staff with id" + id + " from database");

}

@Override
public ResponseEntity<List<StudentProspects>> filterStudentProspects(boolean status)
{
   
    if(status==true)
    {
    return ResponseEntity.ok(studentprospectsrepository.findByActive(status));
    }
    else
    {
        return ResponseEntity.ok(studentprospectsrepository.findByPassive(status));
    }
}
}