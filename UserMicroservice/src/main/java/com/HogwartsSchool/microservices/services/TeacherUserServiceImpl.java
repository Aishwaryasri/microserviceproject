package com.HogwartsSchool.microservices.services;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.HogwartsSchool.microservices.exceptions.TeacherNotFoundException;
import com.HogwartsSchool.microservices.model.Assessments;
import com.HogwartsSchool.microservices.model.PatchAddressAndContactUpdate;
import com.HogwartsSchool.microservices.model.StudentResults;
import com.HogwartsSchool.microservices.model.Teacher;
import com.HogwartsSchool.microservices.repositories.AssessmentRepository;
import com.HogwartsSchool.microservices.repositories.StudentResultsRepository;
import com.HogwartsSchool.microservices.repositories.TeacherRepository;


@Service
public class TeacherUserServiceImpl implements TeacherUserService {
	
	@Autowired TeacherRepository  teacherepository;
	@Autowired AssessmentRepository assessmentsrepository;
	@Autowired	StudentResultsRepository studentresultsrepository;

	@Override
	public ResponseEntity<List<Teacher>> getAllTeachers() {
		return ResponseEntity.ok(teacherepository.findAll()) ;
	}

	@Override
	public ResponseEntity<Teacher> insertTeacher(Teacher teacher) {
		teacherepository.save(teacher);
		URI location =ServletUriComponentsBuilder
		.fromCurrentRequest()
		.path("/{id}")
		.buildAndExpand(teacher.getTeacherId()).toUri();
		
		return ResponseEntity.created(location).body(teacher);
	}

	@Override
	public ResponseEntity<Teacher> findTeacherById(int id) {
		Teacher teacher = teacherepository.findById(id)
				.orElseThrow(()-> new TeacherNotFoundException(id));	
		return ResponseEntity.ok(teacher);
	}

	@Override
	public ResponseEntity<Teacher> fullUpdateTeacherById(Teacher teacher, int id) {
		Teacher teacherdetail = teacherepository.findById(id).orElseThrow(()-> new TeacherNotFoundException(id));
		teacherdetail.setTeacherId(teacher.getTeacherId());
		teacherdetail.setFirstName(teacher.getFirstName());
		teacherdetail.setLastName(teacher.getLastName());
		teacherdetail.setDateOfBirth(teacher.getDateOfBirth());
		teacherdetail.setGender(teacher.getGender());
		teacherdetail.setContactNumber(teacher.getContactNumber());
		teacherdetail.setQualification(teacher.getQualification());
		teacherdetail.setExperienceInYears(teacher.getExperienceInYears());
        teacherdetail.setAddress(teacher.getAddress());
		

		Teacher updatedcourse=teacherepository.save(teacherdetail);
		return ResponseEntity.ok(updatedcourse);
	}

	@Override
	
	 public ResponseEntity<Teacher> partialUpdateTeacherById(PatchAddressAndContactUpdate teacherDetails, @PathVariable int id)
	    {
	        Teacher teacher=teacherepository.findById(id).orElseThrow(()->new TeacherNotFoundException(id));
	        teacher.setAddress(teacherDetails.getAddress());
	        teacher.setContactNumber(teacherDetails.getContactNumber());
	        Teacher updatedTeacher= teacherepository.save(teacher);
	        return ResponseEntity.ok(updatedTeacher);
	    }
    
	
	@Override
	public ResponseEntity<?> deleteTeacherById(int id) {
		Teacher teacher =teacherepository.findById(id).orElseThrow(()-> new TeacherNotFoundException(id));
		teacherepository.delete(teacher);
		return ResponseEntity.ok().body("successfully deleted student with id" + id + " from database");
	
	}

	@Override
    public ResponseEntity<Assessments> insertAssessment(Assessments Assessment){
		assessmentsrepository.save(Assessment);
	        URI location =ServletUriComponentsBuilder
	                .fromCurrentRequest()
	                .path("/{id}")
	                .buildAndExpand(Assessment.getAssessmentId()).toUri();
	        return ResponseEntity.created(location).body(Assessment);
	    }
	
	@Override
	public ResponseEntity<StudentResults> insertAssessmentResults(StudentResults studentResults){
		studentresultsrepository.save(studentResults);
	        URI location =ServletUriComponentsBuilder
	                .fromCurrentRequest()
	                .path("/{id}")
	                .buildAndExpand(studentResults.getStudentResultId()).toUri();
	        return ResponseEntity.created(location).body(studentResults);
	    }

	@Override
	public ResponseEntity<List<Assessments>> getAllAssessment() {
		return ResponseEntity.ok(assessmentsrepository.findAll()) ;
	}   
	

	
		
	
	}

	


	
	
	

