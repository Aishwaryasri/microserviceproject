package com.HogwartsSchool.microservices.services;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import com.HogwartsSchool.microservices.model.Assessments;
import com.HogwartsSchool.microservices.model.PatchAddressAndContactUpdate;
import com.HogwartsSchool.microservices.model.StudentResults;
import com.HogwartsSchool.microservices.model.Teacher;



public interface TeacherUserService {

	// Teacher Methods
		public ResponseEntity<List<Teacher >> getAllTeachers();
		public ResponseEntity< Teacher > insertTeacher (Teacher teacher);
		public ResponseEntity< Teacher > findTeacherById(int id);
		public ResponseEntity< Teacher > fullUpdateTeacherById (Teacher teacher,int id);
		public ResponseEntity<Teacher> partialUpdateTeacherById(PatchAddressAndContactUpdate teacherDetails, @PathVariable int id);
		public ResponseEntity <?> deleteTeacherById( int id);
	    public ResponseEntity<Assessments> insertAssessment(Assessments Assessment);
		public ResponseEntity<StudentResults> insertAssessmentResults(StudentResults studentResults);
		public ResponseEntity<List<Assessments>> getAllAssessment();
	
}
