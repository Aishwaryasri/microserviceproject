package com.HogwartsSchool.microservices.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.HogwartsSchool.microservices.validations.MinimumAge;



@Entity
@Table(name ="studentprospects")
public class StudentProspects {

@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name ="prospectid", nullable = false)
private int prospectId;

@Column(name =	"firstname", nullable = false)
private String firstName;
	
@Column(name =	"lastname", nullable = false)
private String lastName;

@MinimumAge(message = " This prospect's age is less than 16 : prospect not eligible" )
@Column(name =	"dateofbirth", nullable = false)
Date dateOfBirth;

@Column(name =	"gender", nullable = false)
private String gender;

@Column(name =	"fathername", nullable = false)
private String fatherName;

@Column(name =	"address", nullable = false)
private String address;

@Column(name =	"contactnumber", nullable = false)
private String contactNumber;

@Column(name =	"status", nullable = false)
private String Status;

public StudentProspects() {
	super();
	// TODO Auto-generated constructor stub
}

public StudentProspects(int prospectId, String firstName, String lastName, Date dateOfBirth, String gender,
		String fatherName, String address, String contactNumber, String status) {
	super();
	this.prospectId = prospectId;
	this.firstName = firstName;
	this.lastName = lastName;
	this.dateOfBirth = dateOfBirth;
	this.gender = gender;
	this.fatherName = fatherName;
	this.address = address;
	this.contactNumber = contactNumber;
	Status = status;
}

public int getProspectId() {
	return prospectId;
}

public void setProspectId(int prospectId) {
	this.prospectId = prospectId;
}

public String getFirstName() {
	return firstName;
}

public void setFirstName(String firstName) {
	this.firstName = firstName;
}

public String getLastName() {
	return lastName;
}

public void setLastName(String lastName) {
	this.lastName = lastName;
}

public Date getDateOfBirth() {
	return dateOfBirth;
}

public void setDateOfBirth(Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public String getFatherName() {
	return fatherName;
}

public void setFatherName(String fatherName) {
	this.fatherName = fatherName;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getContactNumber() {
	return contactNumber;
}

public void setContactNumber(String contactNumber) {
	this.contactNumber = contactNumber;
}

public String getStatus() {
	return Status;
}

public void setStatus(String status) {
	Status = status;
}

}
