package com.HogwartsSchool.microservices.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
@Entity
@Table(name="student")
public class Student {

   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
    
   @Column(name="studentid", nullable = false)
   private int studentId;
   
   @Column(name="firstname",nullable=false)
   private String firstName;
   
   @Column(name="lastname",nullable=false)
   private String lastName;
   
   @Column(name="dateofbirth",nullable=false)
   @JsonFormat(pattern="yyyy-MM-dd")
   Date dateOfBirth;
   
   @Column(name="gender",nullable=false)
   private String gender;
   
   @Column(name="fathername",nullable=false)
   private String fatherName;
   
   @Column(name="address",nullable=false)
   private String address;
   
   @Column(name="contactnumber",nullable=false)
   private String contactNumber;

public Student() {
	super();
	// TODO Auto-generated constructor stub
}

public Student(int studentId, String firstName, String lastName, Date dateOfBirth, String gender, String fatherName,
		String address, String contactNumber) {
	super();
	this.studentId = studentId;
	this.firstName = firstName;
	this.lastName = lastName;
	this.dateOfBirth = dateOfBirth;
	this.gender = gender;
	this.fatherName = fatherName;
	this.address = address;
	this.contactNumber = contactNumber;
}

public int getStudentId() {
	return studentId;
}

public void setStudentId(int studentId) {
	this.studentId = studentId;
}

public String getFirstName() {
	return firstName;
}

public void setFirstName(String firstName) {
	this.firstName = firstName;
}

public String getLastName() {
	return lastName;
}

public void setLastName(String lastName) {
	this.lastName = lastName;
}

public Date getDateOfBirth() {
	return dateOfBirth;
}

public void setDateOfBirth(Date dateOfBirth) {
	this.dateOfBirth = dateOfBirth;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public String getFatherName() {
	return fatherName;
}

public void setFatherName(String fatherName) {
	this.fatherName = fatherName;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getContactNumber() {
	return contactNumber;
}

public void setContactNumber(String contactNumber) {
	this.contactNumber = contactNumber;
}
  
   
}
