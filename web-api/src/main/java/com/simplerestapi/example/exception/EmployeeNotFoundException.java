package com.simplerestapi.example.exception;

public class EmployeeNotFoundException extends Exception {
		private Integer employeeid;
		public EmployeeNotFoundException(Integer employeeid) {
		        super(String.format("employee is not found with id : '%s'",employeeid));
		        }
		}
