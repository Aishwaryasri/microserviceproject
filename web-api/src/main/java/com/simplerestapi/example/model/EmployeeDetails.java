package com.simplerestapi.example.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employeeid")
public class EmployeeDetails {

    public EmployeeDetails() {
 
    }
 
    public EmployeeDetails(Integer employeeid, String firstName, String lastName, String email) {
        super();
        this.employeeid = employeeid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
    @Id
    @GeneratedValue
    private Integer employeeid;
    private String firstName;
    private String lastName;
    private String email;
	public Integer getEmployeeid() {
		return employeeid;
	}
	//Getters and setters
	public void setEmployeeid(Integer employeeid) {
		this.employeeid = employeeid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
 
    
 
  