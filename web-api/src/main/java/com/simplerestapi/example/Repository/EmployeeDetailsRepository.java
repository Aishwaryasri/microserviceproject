package com.simplerestapi.example.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.simplerestapi.example.model.EmployeeDetails;

     @Repository
	public interface EmployeeDetailsRepository extends JpaRepository<EmployeeDetails, Integer > {

}
